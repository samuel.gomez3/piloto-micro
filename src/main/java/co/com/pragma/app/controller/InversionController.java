package co.com.pragma.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/inversion")
public class InversionController {

	@PostMapping
	public ResponseEntity<String> subirInversion(@RequestParam("file") MultipartFile file,
			@RequestParam("initialValue") String initialValue, @RequestParam("interestDays") String interestDays) {
		String fileName = file.getOriginalFilename();

		return ResponseEntity.ok("Archivo recibido: " + fileName + initialValue + " - " + interestDays);
	}

}
