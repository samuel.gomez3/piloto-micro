package co.com.pragma.app.entities;

import lombok.Data;

@Data
public class Inversion {

	double valorFinal;
	double tasaInteres;
}
