package co.com.pragma.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PilotoMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(PilotoMicroApplication.class, args);
	}

}
